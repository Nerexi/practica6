package com.example.dell.practicasensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ActividadSensorAcelerometro extends AppCompatActivity implements SensorEventListener {
    TextView texto,texto1,texto2 ;
    SensorManager sensorManager;
    private Sensor acelerometro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_acelerometro);

        texto=(TextView)findViewById(R.id.lblTexto);
        texto1=(TextView)findViewById(R.id.lblTexto1);
        texto2=(TextView)findViewById(R.id.lblTexto2);


        sensorManager =(SensorManager) getSystemService(SENSOR_SERVICE);
        acelerometro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);



    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float x,y,z;
        x=event.values[0];
        y=event.values[1];
        z=event.values[2];
        texto.setText("");
        texto.append("\n El valor de x:"+ x );
        texto1.setText("");
        texto1.append("\n El valor de y:"+ y);
        texto2.setText("");
        texto2.append("\n El valor de z:"+ z);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this,acelerometro,sensorManager.SENSOR_DELAY_NORMAL);
    }
    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

}
